import json
import asyncio
import logging
from xml.dom.minidom import parseString

log = logging.getLogger()

async def read_data(request, data_format):
	'''
	Общий обработчик для всех форматов поступающих данных. Исключения, образовавшиеся в этом блоке, будут проброшены в 
	вызывающую обработки обновлений и успешно проигнорированы
	'''
	parsed_data = {}
	if data_format == 'csv':
		log.debug('using csv reader')
		parsed_data = await csv_reader(request)
	elif data_format == 'json':
		log.debug('using json reader')
		parsed_data = await json_reader(request)
	elif data_format == 'xml':
		log.debug('using xml reader')
		parsed_data = await xml_reader(request)
	return parsed_data

async def csv_reader(request):
	data = {}
	async for line in request.content:
		# print(line)
		from_curr, to_curr, rate = line.decode("utf-8").split(',')
		data[(from_curr, to_curr)] = float(rate)
	# print(data)
	return data

async def json_reader(request):
	return {(item['from'], item['to']) : float(item['rate']) for item in await request.json()}
	'''
	Внутри обработчика json вызывается синхронная функция json.loads, которая потенциально может приостановить 
	выполнение кода. Можно написать асинхронный парсер json, однако на данный момент это кажется избыточно сложным. 
	Аналогичная ситуация с обработчиком xml. На данный момент для больших запросов на обновление лучше использовать формат csv
	'''
	
async def xml_reader(request):
	data = {}
	xml = parseString(await request.text())
	conversions = xml.getElementsByTagName('conversion')
	for conversion in conversions:
		data[(conversion.attributes['from'].value, conversion.attributes['to'].value)] = float(conversion.attributes['rate'].value)
	return data

def sync_csv_reader(filepath):
	data = {}
	with open(filepath, 'r') as input:
		for line in input:
			from_curr, to_curr, rate = line.split(',')
			data[(from_curr, to_curr)] = float(rate)
	return data