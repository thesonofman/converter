import asyncio
import traceback
import concurrent
import logging
from readers import read_data

async def run_autosave(data):
	try:
		proc = await asyncio.create_subprocess_shell(
	        "python3 autosave.py",
	        stdin=asyncio.subprocess.PIPE,
	        stdout=asyncio.subprocess.PIPE,
	        stderr=asyncio.subprocess.PIPE)
		for key in data:
			line = "{},{},{}\n".format(key[0], key[1], data[key])
			proc.stdin.write(line.encode('utf-8'))
			await proc.stdin.drain()
		proc.stdin.write_eof()
		stdout, stderr = await proc.communicate()
	except asyncio.CancelledError:
		try:
			proc.kill()
		except Exception:
			pass

class UpdatesProcessor():
	'''
	Класс, содержащий очередь обновлений, методы работы с обновлениями. Также было принято решение расположить внутри объекта класса
	данные о курсах, так как очередь обновлений и хранилище данных тесно взаимодействуют между собой.
	'''
	# logger = logging.getLogger('UpdatesProcessor')
	# logger.addHandler(logging.StreamHandler())

	def __init__(self):
		self._update_queue = asyncio.Queue()
		self._conversion_data = {}
		self.log = logging.getLogger()

	async def queue_processor(self):
		'''
		Обработчик запросов на обновление данных. С помощью asyncio.Queue была сделана попытка упорядочить обновления в 
		порядке их поступления. Все ошибки, связанные с парсингом обновления и его применением подавляются
		'''
		try:
			while True:
				try:
					# global _conversion_data

					update = await self._update_queue.get()
					###
					self.log.info('started processing update')
					update['data'] = await read_data(update['body'], update['format'])
					###
					self.log.debug(update['data'])
					if int(update['merge']) == 0:
						self._conversion_data = update['data']
					elif int(update['merge']) == 1:
						self._conversion_data.update(update['data'])
					###
					self.log.info('updated')
					self.log.debug(self._conversion_data)
				except asyncio.CancelledError:
					raise
				except Exception as error:
					###
					# self.logger.debug('an error occured')
					self.log.exception(error)
					pass
		except asyncio.CancelledError:
			pass

	async def autosave(self):
		'''
		Функция автосохранения на случай отключения сервиса. Параметры частоты и максимальной длительности обновления могут
		быть изменены. Запись сохранения в файл производится в отдельном процессе, данные в который передаются через PIPE.
		'''
		_AUTOSAVE_INTERVAL = 60
		_AUTOSAVE_TIME = 15
		try:
			while True:
				try:
					await asyncio.sleep(_AUTOSAVE_INTERVAL)
					self.log.debug('saving')
					'''
					Данные копируются при передаче в функцию сохранения, для избежания их изменения в процессе записи в файл.
					Здесь может быть использована конструкция async for для итерирования по записям словаря данных, но в этом случае 
					данные могут измениться на середине сохранения 
					'''
					await asyncio.wait_for(run_autosave({**self._conversion_data}), _AUTOSAVE_TIME)
				except concurrent.futures._base.CancelledError:
					raise
				except asyncio.CancelledError:
					raise
				except Exception as error:
					self.log.exception(error)
					pass 
		except concurrent.futures._base.CancelledError:
			pass
		except asyncio.CancelledError:
			pass