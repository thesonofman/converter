'''
Автор: Галабурда Петр
Дата: 03.03.2019
Описание:

В данном файле представлен веб-сервис, обрабатывающий запросы на перевод между валютами. Сервис не использует 
БД для хранения данных, все данные хранятся в памяти в хэш-таблице с данными. Сервис поддерживает два типа запросов:

1) GET-запрос на рассчет перевода между валютами 
Пример запроса: /convert?from=RUR&to=USD&amount=42
Ответ поступает в формате JSON в виде {"amount" : <float>}

2) POST-запрос на обновление или замену базы данных
Пример запроса: /database?format=xml&merge=1

Пример xml-тела запроса:
	<conversions>
		<conversion from="USD" to="RUR" rate="65"></conversion>
		<conversion from="EUR" to="RUR" rate="80"></conversion>
	</conversions>

Пример csv-тела запроса:
	USD,RUR,62
	EUR,RUR,75
	BYN,RUR,33

Пример json-тела запроса:
	[
		{"from" : "USD", "to" : "RUR", "rate" : "63"}
	]

Ответом на запрос всегда является код HTTP 200 с пустым содержанием, однако это не гарантирует успешное обновление.
Порядок применения обновлений совпадает с порядком поступления запросов на обновление

Зависимости и требования к системе перечислены в файле Pipfile

Запуск сервера производится командой 'python3 converter_server.py'
'''
import os
import logging
import asyncio
from aiohttp import web
from readers import read_data, sync_csv_reader
from background import run_autosave, UpdatesProcessor

routes = web.RouteTableDef()
updates_processor = UpdatesProcessor()
streamHandler = logging.StreamHandler()
logging.basicConfig(format='%(levelname)s|\t|%(asctime)s|\t|%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO, handlers = [streamHandler])
'''
Логирование производится в консоль, уровнем логирования по умолчанию является INFO. Для более подробных логов или перенаправления 
логирования в поток или файл соответствующие изменения вносятся в код здесь.
'''

log = logging.getLogger()

@routes.get('/convert')
async def convert(request):
	'''
	GET метод для перевода между валютами. Запрос имеет сложность O(1) на доступ к хэш-таблице с данными о курсах валют
	'''
	params = dict(request.query)
	try:
		response = float(params['amount']) * updates_processor._conversion_data[(params['from'], params['to'])]
	except KeyError:
		raise web.HTTPBadRequest() #FIXME: корректно отреагировать на неправильные параметры
	return web.json_response({'amount' : float(params['amount']) * updates_processor._conversion_data[(params['from'], params['to'])]})

@routes.post('/database')
async def convert(request):
	'''
	POST метод для обновления базы данных. Все обновления добавляются в очередь обновлений и применяются в порядке поступления
	'''
	params = dict(request.query)
	'''
	Передача параметра params может быть излишней, однако была сделана для прозрачности интерфейса.
	'''
	update = {**params, 'body' : request}
	# global _update_queue
	await updates_processor._update_queue.put(update)
	'''
	Запрос на обновление всегда возвращает ответ 200, так как ожидание обновления может занимать много времени. 
	К тому же предполагается, что сервер может сформировать корректное тело csv или json запроса
	'''
	return web.Response(body = None, status = 200)



async def start_background_tasks(app):
	'''
	Добавление background процессов обработки очереди и автосохранения при запуске
	'''
	app['queue_processor'] = app.loop.create_task(updates_processor.queue_processor())
	app['autosave'] = app.loop.create_task(updates_processor.autosave())

async def cleanup_background_tasks(app):
	'''
	Остановка background процессов при остановке сервиса. В самих процессах исключение asyncio.CancelledError подавляется, поэтому
	в связи с их остановкой не должно возникнуть проблем
	'''
	app['queue_processor'].cancel()
	app['autosave'].cancel()
	await app['queue_processor']
	await app['autosave']

'''
Сборка приложения: 
	- добавление обработчиков запросов
	- добавление задач для выполнения при старте сервера
	- добавление задач для выполнения при остановке сервера
'''
app = web.Application()
app.add_routes(routes)
app.on_startup.append(start_background_tasks)
app.on_cleanup.append(cleanup_background_tasks)

'''
Попытка загрузиться из последнего файла сохранения. Загрузка из сохраненных данных выполняется синхронно, так как до завершения загрузки 
сервер может некорректно реагировать на поступающие запросы
'''
try:
	last_save = max(os.listdir(path='saves'))
	log.warning("loading from savefile '{}'".format(last_save))
	updates_processor._conversion_data = sync_csv_reader("saves/{}".format(last_save))
except FileNotFoundError:
	os.mkdir('saves')
except ValueError:
	log.warning("error, loading with no savefile")
	updates_processor._conversion_data = {}

log.debug(updates_processor._conversion_data)

'''
Запуск сервера
'''
web.run_app(app)