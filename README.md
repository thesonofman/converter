# Converter
## Описание:

В данном файле представлен веб-сервис, обрабатывающий запросы на перевод между валютами. Сервис не использует 
БД для хранения данных, все данные хранятся в памяти в хэш-таблице с данными. Сервис поддерживает два типа запросов:

1. GET-запрос на рассчет перевода между валютами 
Пример запроса: ```/convert?from=RUR&to=USD&amount=42```
Ответ поступает в формате JSON в виде ```{"amount" : <float>}```

2. POST-запрос на обновление или замену базы данных
Пример запроса: ```/database?format=xml&merge=1```

Пример xml-тела запроса:
```xml
	<conversions>
		<conversion from="USD" to="RUR" rate="65"></conversion>
		<conversion from="EUR" to="RUR" rate="80"></conversion>
	</conversions>
```

Пример csv-тела запроса:
```
	USD,RUR,62
	EUR,RUR,75
	BYN,RUR,33
```

Пример json-тела запроса:
```json
	[
		{"from" : "USD", "to" : "RUR", "rate" : "63"}
	]
```

Ответом на POST запрос всегда является код HTTP 200 с пустым содержанием, однако это не гарантирует успешное обновление.
Порядок применения обновлений совпадает с порядком поступления запросов на обновление

Запуск сервера производится командой:
```sh
python3 converter_server.py
```
##### Зависимости и требования к системе перечислены в файле Pipfile
##### Задание описано в файле currency.md
#
## PS
В файлах с исходным кодом достаточно много комментариев, однако некоторые мысли, которые я не изложил там, я напишу здесь

1. Для разработки была использована библиотека asyncio, хотя я имею больше опыта разработки на Django по некоторым конкретным причинам:

    - Насколько я знаю, Django работает в одном потоке синхронно, и обычно запускается несколько синхронных процессов, которые работают независимо. В то же время в работе хранилище данных находится в памяти, и обмен данными между несколькими процессами вызвал бы определенные трудности. При запуске асинхронного сервиса данные используются одним потоком, поэтому нет надобности в потокобезопасных структурах данных, которые могли бы замедлить работу сервиса. К тому же при поступлении большого обновления данных синхронный сервер приостановит обработку запросов на время парсинга большого json или xml, что также будет являться проблемой
    - Django показался мне слишком тяжелым инструментом для решения такой задачи. Встроенные механизмы Django могут оказаться очень полезны при реализации больших проектов, однако вряд ли подойдут для системы, основанной на множестве микросервисов, в которых HTTP является средством коммуникации внутри системы. Однако я могу ошибаться
    
2. Тестирование приложения осуществлялось приложением Postman, которое позволяет отправлять заготовленные наборы HTTP запросов и автоматически проверять ответы на них. Таким образом проводилось своеобразное unit-тестирование приложения, поэтому в коде нет модуля с unit-тестами и модуль unittest не использовался. Однако я бы хотел вповледствии узнать, каким образом проводится unit-тестирование подобных веб-сервисов
3. Несмотря на то, что я приложил все усилия к тому, чтобы написать пристойное асинхронное приложение, в коде программы все еще есть участки, где управление может долго не передаваться другим задачам, например парсинг больших обновлений формата json, для которых используется синхронный json.loads(). Хотелось бы узнать, как такие моменты обходятся в хорошем асинхронном коде